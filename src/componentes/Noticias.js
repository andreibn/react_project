import React from 'react';
import imagenes from './ImagenesNoticias';

export const Noticias = (props) => {

    return (
        <div className="contenedor_noticia">
            <div className="info_noticias"><p className="texto_noticias">{props.titulo}</p></div>
            <img src={imagenes[props.imagen-1]} alt="noticias" className="img_noticia" />
        </div>
    );
}