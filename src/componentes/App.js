import React, { useState } from 'react';
import '../sass/App.scss';
import gif from '../gif/ezgif.com-gif-maker.mp4';
import { MenuLateral } from './MenuLateral';
import { BotonMenuLateral } from './BotonMenuLateral';
import { Noticias } from './Noticias';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faEnvelopeOpen, faMapMarkerAlt, faPhone } from '@fortawesome/free-solid-svg-icons';
import counterpart from 'counterpart'; 
import Translate from 'react-translate-component';
import es from '../lenguajes/es'
import en from '../lenguajes/en'
import pt from '../lenguajes/pt'
import { Bandera } from './Banderas'
//librería react-translate-component doc: https://www.npmjs.com/package/react-translate-component




export const App = () => {
  const [localLanguage, setCurrentLocalLanguage] = useState(1);
  counterpart.registerTranslations('es',es);
  counterpart.registerTranslations('en',en);
  counterpart.registerTranslations('pt',pt);
  var localLang = 'es';
  counterpart.setLocale(localLang);
  function changeLanguage(lang)
  {
    counterpart.setLocale(lang);
    localLang = lang;
    componentDidMount(){
      setCurrentLocalLanguage(lang);

    }
    }
  }


    return ( 
      <div>
        <header className="header">
        <BotonMenuLateral />
          <div className="header_enlaces">
            <ul className="header_enlaces-ul"> 
              <li className="header_enlaces-li"><a href="#seccion-Inicio" className="header_enlaces-a"><Translate content="inicio"/></a></li>
              <li className="header_enlaces-li"><a href="#seccion-Noticias" className="header_enlaces-a"><Translate content="ult_noticias"/></a></li>
              <li className="header_enlaces-li"><a href="#seccion-Objetivos" className="header_enlaces-a"><Translate content="objetivos"/></a></li>
              <li className="header_enlaces-li"><a href="#seccion-Contactanos" className="header_enlaces-a"><Translate content="contactanos"/></a></li>
              <i><Bandera lang='es' onClick={changeLanguage} isLocalLanguage={localLanguage === 'es'}/></i>
              <i><Bandera lang='en' onClick={changeLanguage} isLocalLanguage={localLanguage === 'en'} /></i>
              <i><Bandera lang='pt' onClick={changeLanguage} isLocalLanguage={localLanguage === 'pt'}/></i>
            </ul>
          </div>

        </header>

         <MenuLateral />
          <a name="seccion-Inicio" href="#section"></a>
          <div className="portada">
            <div className="portada_texto">
              <h1 className="portada_texto-titulo">SearchForIt</h1>
              <a className="portada_texto-boton" href="#seccion-Informacion"><Translate content="sobre"/></a>
            </div>
          </div>
          <a name="seccion-Noticias" href="#section"></a>
          <div className="noticias">
            <div className="noticias-titulo-contenedor">
              <h2 className="noticias-titulo"><Translate content="noticias_destacadas"/></h2>
            </div>
            <div className="noticias_destacadas-contenedor">
              <div className="noticias_destacadas-superior">
                <Noticias imagen="1" titulo="Empiezan las elecciones presidenciales en EEUU 2020" />
                <Noticias imagen="2" titulo="Se aprueba el Estado de Alarma a partir del 14 de marzo de 2020 en España" />
                <Noticias imagen="3" titulo="Joe Biden gana las elecciones presidenciales de EEUU" />
                <Noticias imagen="4" titulo="Los casos de coronavirus bajan y disminuye la presión hospitalaria" />
              </div>
              <div className="noticias_destacadas-inferior">
                <Noticias imagen="5" titulo="La NASA confirma la existencia de agua en la superficie de la Luna" />
                <Noticias imagen="6" titulo="Fallece el futbolista Diego Armando Maradona por una insuficiencia cardíaca aguda" />
                <Noticias imagen="7" titulo="Las nuevas vacunas del COVID-19 podrían llegar a principios de 2021" />
                <Noticias imagen="8" titulo="Sanidad impone el confinamiento perimetral a Madrid y otros municipios" />
              </div>
            </div>
          </div>
          <a name="seccion-Informacion" href="#section"></a>
          <div className="seccion_informacion">
            <div className="seccion_informacion-titulo-contenedor">
            <h2 className="seccion_informacion-titulo"><Translate content="consiste"/></h2>
            </div>
            <div className="seccion_informacion-contenedor">
              <div className="seccion_informacion-contenedor-texto">
                <div className="seccion_informacion-texto">
                  <h2 className="seccion_informacion-texto-titulo"><Translate content="que_es"/></h2>
                  <p className="seccion_informacion-texto-texto">Lorem ipsum dolor sit amet, consectetur adipisicing elit dicta illo eveniet ullam. Fuga suscipit ipsam distinctio saepe officia possimus repellendus quaerat animi non optio exercitationem, dolores unde. Lorem ipsum dolor sit amet, consectetur adipisicing elit dicta illo eveniet ullam. Fuga suscipit ipsam distinctio saepe officia possimus repellendus quaerat animi non optio exercitationem, dolores unde.</p>
                </div>
                <div className="seccion_informacion-texto">
                  <h2 className="seccion_informacion-texto-titulo"><Translate content="a_que_nos_dedicamos"/></h2>
                  <p className="seccion_informacion-texto-texto">Lorem ipsum dolor sit amet, consectetur adipisicing elit dicta illo eveniet ullam. Fuga suscipit ipsam distinctio saepe officia possimus repellendus quaerat animi non optio exercitationem, dolores unde. Lorem ipsum dolor sit amet, consectetur adipisicing elit dicta illo eveniet ullam. Fuga suscipit ipsam distinctio saepe officia possimus repellendus quaerat animi non optio exercitationem, dolores unde.</p>
                </div>
              </div>
              <div className="seccion_informacion-imagen">
                <video className='gif_noticias' autoPlay loop muted>
                  <source src={gif} type='video/mp4' />
                </video>
              </div>
            </div>
          </div>
          <a name="seccion-Objetivos" href="#section"></a>
          <div className="seccion_objetivos">
            <div className="seccion_objetivos-titulo"><Translate content="nuestros_objetivos"/></div>
            <div className="seccion_objetivos-contenedores">
              <div className="seccion_objetivos-contenedor">
                <h3 className="seccion_objetivos-contenedor-titulo"><Translate content="informar"/></h3>
                <p className="seccion_objetivos-contenedor-texto">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore laudantium sit eum non nam.</p>
              </div>
              <div className="seccion_objetivos-contenedor">
                <h3 className="seccion_objetivos-contenedor-titulo"><Translate content="ser_imparcial"/></h3>
                <p className="seccion_objetivos-contenedor-texto">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore laudantium sit eum non nam.</p>
              </div>
              <div className="seccion_objetivos-contenedor">
                <h3 className="seccion_objetivos-contenedor-titulo"><Translate content="entretener"/></h3>
                <p className="seccion_objetivos-contenedor-texto">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore laudantium sit eum non nam.</p>
              </div>
              <div className="seccion_objetivos-contenedor">
                <h3 className="seccion_objetivos-contenedor-titulo"><Translate content="culturas"/></h3>
                <p className="seccion_objetivos-contenedor-texto">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore laudantium sit eum non nam.</p>
              </div>
            </div>
          </div>
        
        <footer className="footer">
          <div className="footer-seccion1">
            <h2 className="footer-titulo"><Translate content="acerca_de"/></h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis eos quae, dolores facilis nesciunt fuga aperiam est corrupti numquam necessitatibus, amet nostrum ipsa autem modi voluptatibus. Saepe sit iure blanditiis!</p>
            <div className="footer-redesSociales">
              <ul className="footer_redesSociales-ul">
                <li className="footer_redesSociales-li">
                  <FontAwesomeIcon icon={faInstagram} className="footer_redesSociales-iconoInstagram" />
                </li>
                <li className="footer_redesSociales-li">
                  <FontAwesomeIcon icon={faFacebook} className="footer_redesSociales-iconoFacebook" />
                </li>
                <li className="footer_redesSociales-li">
                  <FontAwesomeIcon icon={faTwitter} className="footer_redesSociales-iconoTwitter" />
                </li>
              </ul>
            </div>
          </div>
          <div className="footer-seccion2">
            <h2 className="footer-titulo"><Translate content="contacto1"/></h2>
            <ul className="footer_contacto-ul">
              <li className="footer_contacto-li"><FontAwesomeIcon icon={faMapMarkerAlt} className="footer_contacto-icono" />Mahadahonda, Madrid</li>
              <li className="footer_contacto-li"><FontAwesomeIcon icon={faPhone} className="footer_contacto-icono" />+34 916-435-534</li>
              <li className="footer_contacto-li"><FontAwesomeIcon icon={faEnvelopeOpen} className="footer_contacto-icono" />searchforit@gmail.com</li>
            </ul>
          </div>
          <div className="footer-seccion3">
            <h2 className="footer-titulo"><Translate content="contacto2"/></h2>
            <div className="footer_formulario">
              <form action="#">
                <label className="footer_formulario-label">Email *</label>
                <hr/>
                <input type="email" required className="footer_formulario-inputEmail"/>
                <hr/>
                <label className="footer_formulario-label"><Translate content="mensaje"/> *</label>
                <hr/>
                <textarea rows='2' cols="25" className="footer_formulario-textArea" required></textarea>             
                <hr/>
                <button type="submit" className="footer_formulario-boton"><Translate content="enviar"/></button>       
              </form>
            </div>
          </div>

        </footer>
      </div>
    );
}
