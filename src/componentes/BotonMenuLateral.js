import React, {useState } from 'react';

export const BotonMenuLateral = () => {

    const [estadoBoton, setEstadoBoton] = useState('botonHamburguesa-cerrado');
    //const [posicionBoton, setPosicionBoton] = useState('botonHamburguesa-cerrado');
//    const [estadoMenuLateral, setEstadoMenuLateral] = useState('menu_lateral-visible');

    function cambiarEstado(){
        setEstadoBoton(estadoBoton === 'botonHamburguesa-abierto' ? 'botonHamburguesa-cerrado' : 'botonHamburguesa-abierto');
        //setEstadoMenuLateral(estadoMenuLateral === 'menu_lateral-visible' ? 'menu_lateral-oculto' : 'menu_lateral-visible');
    }

    //function moverContenedorBoton{
        //setPosicionBoton(posicionBoton === 'botonHamburguesa-abierto' ? 'Izquierda' : 'Derecha');
    //}

    return(
        <div className="contenedor_hamburguesa">
            <button className="botonHamburguesa" onClick={() => cambiarEstado()}>
                <span className={estadoBoton}></span>
                <span className={estadoBoton}></span>
                <span className={estadoBoton}></span>
            </button>
        </div>
    );
};